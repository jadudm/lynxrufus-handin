DB=$1

if [ "$#" -ne 1 ]; then
  echo Include database path.
  exit 0
fi
  
sqlite3 ${DB} 'drop table assessments;' ""
sqlite3 ${DB} 'drop table rubrics;' ""
sqlite3 ${DB} 'drop table users;' ""
sqlite3 ${DB} 'drop table assignments;' ""
sqlite3 ${DB} 'drop table uploads;' ""
sqlite3 ${DB} 'drop table courses;' ""
sqlite3 ${DB} 'drop table courseuser;' ""

sqlite3 ${DB} 'create table users (email text, last text, first text, secret text, delivered text, primary key(email));' ""
sqlite3 ${DB} 'create table courseuser (email text, cid text, role text, primary key(email, cid));' ""

sqlite3 ${DB} "insert into users (email, last, first, secret, delivered) values ('mjadud@bates.edu', 'Jadud', 'Matt', 'ABCDEFGHIJ', 'NO');" ""
sqlite3 ${DB} 'insert into courseuser (email, cid, role) values ("mjadud@bates.edu", "dcs102w18", "ADMIN")' ""
sqlite3 ${DB} 'insert into courseuser (email, cid, role) values ("mjadud@bates.edu", "dcs202w18", "ADMIN")' ""
