## Setting keys up
racket hadmin.rkt -a mjadud@bates.edu -t 8675309 renew-secret <stu-email>

## Loading Courses
racket hadmin.rkt -a mjadud@bates.edu -t 8675309 load-courses courses.json

racket hadmin.rkt -a mjadud@bates.edu -t 8675309 load-courses <course info file>